FROM node:lts

RUN apt update && \
  apt install -y libnss3 libatk-bridge2.0-0 libdrm2 libxkbcommon0 libgbm1 libgtk-3-0 libasound2 && \
  rm -rf /var/lib/apt/lists/*

ENTRYPOINT [""]
