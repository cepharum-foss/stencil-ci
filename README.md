# Stencil Ci

docker image for testing and building Stencil-based components in GitLab CI

## Configuration example

Add this job to .gitlab-ci.yml file of a Stencil project:

```yaml
test:
    image: registry.gitlab.com/cepharum-foss/stencil-ci
    script:
        - npm ci
        - npx stencil test --spec --e2e
```

In addition, the following configuration must be part of your **stencil.config.ts** file (there should be more configuration stuff, but this is the part essential to CI-based testing in GitLab):

```typescript
import { Config } from '@stencil/core';

export const config: Config = {
  testing: {
    browserArgs: process.env.CI ? ['--no-sandbox', '--disable-setuid-sandbox'] : undefined,
  },
};
```
